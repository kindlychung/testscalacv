name := "testscalacv"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= List(
  "com.typesafe.akka" %% "akka-actor" % "2.4.17",
  "com.typesafe.akka" %% "akka-stream" % "2.4.17",
  "net.codingwell" %% "scala-guice" % "4.1.0"

)
