import org.bytedeco.javacv.FrameGrabber.ImageMode
import org.bytedeco.javacv.OpenCVFrameGrabber

/**
  * Created by kaiyin on 3/8/17.
  */
object GrabVid extends App {
  val grabber = new OpenCVFrameGrabber(0)
  grabber.setImageWidth(800)
  grabber.setImageHeight(800)
  grabber.setBitsPerPixel(8)
  grabber.setImageMode(ImageMode.GRAY)
  grabber.start()

}
