
import com.google.inject.{AbstractModule, PrivateModule}
import net.codingwell.scalaguice.{ScalaModule, ScalaPrivateModule}
import org.bytedeco.javacv.FrameGrabber
import com.typesafe.config.ConfigFactory
import org.bytedeco.javacv.FrameGrabber.ImageMode

class VCModules extends AbstractModule with ScalaModule {
  val config = ConfigFactory.defaultApplication()
  val g = FrameGrabber.createDefault(config.getInt("akka.frameGrabber.deviceID"))
  val imageModeMap = Map (
    "gray" -> ImageMode.GRAY,
    "color" -> ImageMode.COLOR,
    "raw" -> ImageMode.RAW
  )
  g.setImageWidth(config.getInt("akka.frameGrabber.imageWidth"))
  g.setImageHeight(config.getInt("akka.frameGrabber.imageHeight"))
  g.setBitsPerPixel(config.getInt("akka.frameGrabber.bitsPerPixel"))
  g.setImageMode(imageModeMap(config.getString("akka.frameGrabber.imageMode")))
  g.start()

  def configure(): Unit = {
    bind[FrameGrabber].toInstance(g)
    bind[WebCamFramePublisher].to
    bind[WebCam].to
  }
}
