import akka.NotUsed
import akka.actor.{ActorSystem, Props}
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.Source
import com.google.inject.{Inject, Singleton}
import org.bytedeco.javacv.Frame

/**
  * Created by kaiyin on 3/8/17.
  */
@Singleton
class WebCam @Inject()(
                        webCamFramePublisher: WebCamFramePublisher
                      ) {
  def source(implicit system: ActorSystem): Source[Frame, NotUsed] = {
    val props = Props(webCamFramePublisher)
    val webcamActorRef = system.actorOf(props)
    val webcamActorPublisher = ActorPublisher[Frame](webcamActorRef)
    Source.fromPublisher(webcamActorPublisher)
  }
}

case class Dimensions(width: Int, height: Int)