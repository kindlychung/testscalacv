import akka.actor.Actor.Receive
import akka.actor.{ActorLogging, DeadLetterSuppression}
import akka.stream.actor.ActorPublisher
import akka.stream.actor.ActorPublisherMessage.{Cancel, Request}
import com.google.inject.{Inject, Singleton}
import org.bytedeco.javacpp.opencv_core.CV_8U
import org.bytedeco.javacv.{Frame, FrameGrabber}
import org.bytedeco.javacv.FrameGrabber.ImageMode

/**
  * Created by kaiyin on 3/8/17.
  */
@Singleton
class WebCamFramePublisher @Inject()(
                                      grabber: FrameGrabber
                                    ) extends ActorPublisher[Frame] with ActorLogging {
  private implicit val ec = context.dispatcher

  private def grabFrame() = {
    Option(grabber.grab())
  }

  private def emitFrames(): Unit = {
    if (isActive & totalDemand > 0) {
      grabFrame().foreach(onNext)
      if (totalDemand > 0) {
        self ! Continue
      }
    }
  }

  override def receive: Receive = {
    case _: Request => emitFrames()
    case Continue => emitFrames()
    case Cancel => onCompleteThenStop()
    case unexpectedMsg => log.warning(s"Unexpected msg: ${unexpectedMsg}")
  }

  private case object Continue extends DeadLetterSuppression

}
